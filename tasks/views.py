from django.shortcuts import render, redirect, get_object_or_404
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = TaskForm()

    context = {"create_task": form}

    return render(request, "tasks/create.html", context)


@login_required
def task_list(request):
    tasks = Task.objects.filter(assignee=request.user)

    context = {
        "task_list": tasks,
    }
    return render(request, "tasks/list.html", context)


def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm(instance=task)

    context = {
        "task_object": task,
        "task_form": form,
    }
    return render(request, "tasks/edit.html", context)
