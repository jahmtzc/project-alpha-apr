from django.forms import ModelForm, SelectDateWidget
from tasks.models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "is_completed",
        )

        widgets = {
            "due_date": SelectDateWidget,
            "start_date": SelectDateWidget,
        }
